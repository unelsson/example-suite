Contributors
------------
    Bret Curtis (psi29a) - Previous version of the Template and Example Suite
    DestinedToDie - Previous version of the Template and Example Suite
    Isak Larborn - Pelagiad font
    Leja Pelc (Mclawliet) - Attribute and skill icons, UI textures
    Matjaž Lamut (Lamoot) - Base .omwgame file, sky models and textures, player model and animations, UI textures
    Nelsson Huotari (unelsson) - Research and improvements to the animation pipeline
